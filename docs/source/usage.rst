Usage
=====

Once the package is :doc:`installed <installation>` where Python can find it, you can use the function ``lic`` directly.

.. code-block:: Python3

                import lic
                import matplotlib.pyplot as plt

                # ... get x and y arrays from somewhere ...

                lic_result = lic.lic(x, y, length=30)

                plt.imshow(lic_result, origin='lower', cmap='gray')
                plt.show()

Find out more about the options by reading the source documentation:

.. code-block:: shell

                pydoc lic.lic

You can also control the seed, i.e., the underlying texture for the lic:

.. code-block:: shell

                pydoc lic.gen_seed

You can run the example from the root folder to see the result:

.. code-block:: shell

                PYTHONPATH="." python3 examples/ex1.py
