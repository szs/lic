API
===

There are two functions in the module :mod:`lic` that you might want to use, :func:`lic.lic`
and :func:`lic.gen_seed`.

lic
---

.. autofunction:: lic.lic

gen_seed
--------

.. autofunction:: lic.gen_seed
