usage: lic [-h] [-l LENGTH] [-o FILE] [-c] [-q] [-v] [-d] [--version]
           data_x_file data_y_file

Line integral convolution (lic) algorithm. Please have a look at the
documentation (https://pypi.org/project/lic/) for further information on how
tho use this software.

positional arguments:
  data_x_file           an npy file containing a 2d numpy.ndarray with the x
                        component of the vector field
  data_y_file           an npy file containing a 2d numpy.ndarray with the y
                        component of the vector field

optional arguments:
  -h, --help            show this help message and exit
  -l LENGTH, --line-length LENGTH
                        the length of the line of the lic (default: 20)
  -o FILE, --output-file FILE
                        the name of the output file, If it is not set, a name
                        will be generated from the names of the input data
                        files. (default: None)
  -c, --enhance-contrast
                        enhance the contrast of the resulting lic image
                        (default: False)
  -q, --quiet           switch off text output except for error messages. This
                        will overwrite -v. (default: False)
  -v, --verbose         more verbose text output (default: False)
  -d, --debug           switch on debug mode. This will show intermediate
                        results and plots, as well as log a lot of debugging
                        information. (default: False)
  --version             show the version of this software
