Command line tool
=================


You will need npy data files (saved using numpy.save) to use lic from the command line:

.. code-block:: shell

                lic data_x.npy data_y.npy -v -l 30 -c

See ``lic --help`` for a full list of options:

.. literalinclude:: help_string.txt
