Installation
============

The installation is straight forward. You can install the package via ``pip``, ``pipenv``, ``poetry``
and alike or by downloading the source from the gitlab repository.

From pypi.org (recommended)
---------------------------

Install by typing

.. code-block:: shell

                pip install lic

or

.. code-block:: shell

                pip install --user lic

if you do not have root access.

Please check the documentations for `pipenv <https://pipenv.pypa.io/en/latest/>`_, and
`poetry <https://python-poetry.org/docs/>`_ for information on how to install packages with these tools.

Once the package is successfully installed, you can :doc:`import it <usage>` into your program
or use the :doc:`command line tool <cli>`.


From gitlab.com
---------------

To get the latest features or contribute to the development, you can clone the whole project using
`git <https://git-scm.com/>`_:

.. code-block:: shell

                git clone https://gitlab.com/szs/lic.git

Now you can, for instance, copy ``lic.py`` over to your project and :doc:`import it directly <usage>`
or use it as a :doc:`command line tool <cli>`.
