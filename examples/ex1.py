import numpy as np  # type: ignore
import matplotlib.pyplot as plt  # type: ignore
import lic

if __name__ == '__main__':
    # calculate and interesting vector field
    # inspired by https://scipy-cookbook.readthedocs.io/items/LineIntegralConvolution.html
    size = 500
    vortex_spacing = 0.5
    extra_factor = 2.

    a = np.array([1, 0]) * vortex_spacing
    b = np.array([np.cos(np.pi / 3), np.sin(np.pi / 3)]) * vortex_spacing
    rnv = int(2 * extra_factor / vortex_spacing)
    vortices = [n * a + m * b for n in range(-rnv, rnv) for m in range(-rnv, rnv)]
    vortices = [(x, y) for (x, y) in vortices
                if -extra_factor < x < extra_factor and -extra_factor < y < extra_factor]

    xs = np.linspace(-1, 1, size).astype(np.float32)[None, :]
    ys = np.linspace(-1, 1, size).astype(np.float32)[:, None]

    x = np.zeros((size, size), dtype=np.float32)
    y = np.zeros((size, size), dtype=np.float32)

    for (ix, iy) in vortices:
        rsq = (xs - ix)**2 + (ys - iy)**2
        x[...] += -(xs - ix) / rsq
        y[...] += (ys - iy) / rsq

    # get the lic image

    lic_result = lic.lic(x, y, length=30)

    plt.imshow(lic_result, origin='lower', cmap='gray')
    plt.show()
