"""⎣⫯ℂ: Line Integral Convolution for numpy Arrays

LICENSE
   MIT (https://mit-license.org/)

COPYRIGHT
   © 2020 Steffen Brinkmann <s-b@mailbox.org>
"""

from .lic_main import lic, run, gen_seed  # noqa: F401
from .lic_main import _check_data, _load_npy_data, _load_seed, _contrast  # noqa: F401
from .lic_main import __version__  # noqa: F401
