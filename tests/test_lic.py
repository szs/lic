import numpy as np  # type: ignore
import pytest  # type: ignore

import lic
from lic import _check_data, _load_npy_data


def test_check_data():
    """test lic._check_data"""
    assert _check_data(np.empty((1,)), np.empty((1, 1))) is False
    assert _check_data(np.empty((1, 1)), np.empty((1,))) is False
    assert _check_data(np.empty((1, 1)), np.empty((1, 2))) is False
    assert _check_data(np.empty((1, 1)), np.empty((1, 1))) is True


def test_lic():
    """test lic.lic"""
    r1 = np.random.random((100, 100)) - 0.5
    r2 = np.random.random((100, 100)) - 0.5
    r3 = np.random.random((50, 50)) - 0.5
    r4 = np.random.random((100,)) - 0.5

    r1[5, 5] = 0.0
    r2[5, 5] = 0.0

    assert lic.lic(r1, r2) is not None
    assert lic.lic(r1, r2, length=5) is not None
    assert lic.lic(r1, r2, kernel=[1, 2, 3, 4, 3, 2, 1]) is not None
    assert lic.lic(r1, r2, contrast=True) is not None

    with pytest.raises(AssertionError):
        lic.lic(r1, r2, length=-2)
    with pytest.raises(AssertionError):
        lic.lic(r1, r3)
    with pytest.raises(AssertionError):
        lic.lic(r1, r4)


def test_load_data():
    """test lic._load_npy_data"""
    _load_npy_data("./data/bx1.0000.npy", "./data/bx2.0000.npy")

    with pytest.raises(FileNotFoundError):
        _load_npy_data("./data/bx1.0000.npy", "DOES_NOT_EXIST.npy")


def test_cli():
    """test the command line tool"""
    lic.run(['data/bx1.0000.npy', 'data/bx2.0000.npy', '-c', '-o', 'test'])
    lic.run(['data/bx1.0000.npy', 'data/bx2.0000.npy', '-c', '-f', '0.5', '-o', 'test.png'])
    lic.run(['data/bx1.0000.npy', 'data/bx2.0000.npy', '-c', '-f', '0.5', '-s', 'data/bx2.0000.npy'])
    lic.run(['data/bx1.0000.npy', 'data/bx2.0000.npy', '-a 2', '-f', '0.2', '-o', 'test'])
    lic.run(['data/bx1.0000.npy', 'data/bx2.0000.npy', '-a 2', '-f', '0.2', '-o', 'test.gif'])


def test_gen_seed():
    """test lic.gen_seed"""
    lic.gen_seed((100, 200),
                 noise='white',
                 points_every=(10, 20),
                 points_size=0,
                 points_alternate=False,
                 combine='sum')
    lic.gen_seed((100, 200),
                 noise='white',
                 points_every=20,
                 points_size=0,
                 points_alternate=True,
                 combine='replace')
